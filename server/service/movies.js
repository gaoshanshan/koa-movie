const Movie = require('mongoose').model('Movie')

const getMovies = async (type, year) => {
    let query = {}
    if (type) {
        query.movieTypes = { $in: [type] }
    }
    if (year) {
        query.year = year
    }
    return await Movie.find(query)
}

const getMovieDetail = async (id) => {
    return Movie.findOne({ _id: id })
}

const getRelativeMovies = async movie => {
    return Movie.find({
        movieTypes: { $in: movie.movieTypes }
    })
}

module.exports = { getMovies, getMovieDetail, getRelativeMovies }