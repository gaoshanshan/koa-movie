const { Router } = require('../decorator/router')
const { resolve } = require('path')

const router = (app) => {
    const routesPath = resolve(__dirname, '../routes')
    const instance = new Router(app, routesPath)

    instance.init()
}
module.exports = { router }
