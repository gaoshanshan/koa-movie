const Koa = require('koa')
const R = require('ramda')
const { join } = require('path')
const views = require('koa-views')
const {
    connect,
    initSchemas
} = require('./db/init')


const MIDDLEWARES = ['router']
const useMiddlewares = app => {
    R.map(
        R.compose(
            R.forEachObjIndexed(
                e => e(app)
            ),
            require,
            name => join(__dirname, `./middlewares/${name}`)
        )
    )(MIDDLEWARES)
}

    ;
(async () => {
    await connect()
    initSchemas()
    const app = new Koa()
    await useMiddlewares(app)
    app.listen(8080)
})()



