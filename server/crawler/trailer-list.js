const url = "https://movie.douban.com/tag/#/?sort=R&range=5,10&tags="

const puppeteer = require('puppeteer')

const sleep = time => new Promise(resolve => {
    setTimeout(resolve, time)
})

;
(async () => {
    let browser, page, result;

    browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        dumpio: false
    })

    page = await browser.newPage()

    await page.goto(url, {
        waitUntil: 'networkidle2'
    })

    await sleep(3000)

    await page.waitForSelector('.more')

    for (let i = 0; i < 1; i++) {
        await sleep(3000)
        await page.click('.more')
    }

    result = await page.evaluate(() => {
        var $ = window.$,
            items = $('.list-wp a'),
            links = []
        if (items.length > 0) {
            items.each((index, item) => {
                var it = $(item),
                    doubanId = it.find('div').data('id'),
                    title = it.find('.title').text(),
                    rate = it.find('.rate').text(),
                    poster = it.find('img').attr('src').replace('s_ratio', 'l_ratio');
                links.push({
                    doubanId,
                    title,
                    rate,
                    poster
                })
            })
        }
        return links;
    })
    browser.close()
    process.send({
        result
    })
    process.exit(0)
})()