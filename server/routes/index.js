
const { getMovies, getRelativeMovies, getMovieDetail } = require('../service/movies')
const { controller, get } = require('../decorator/router')

@controller('/api/movie')
class MovieController {
    @get('/all')
    async getAll(ctx, next) {
        const { type, year } = ctx.query
        const movies = await getMovies(type, year)

        ctx.body = {
            movies,
            success: true
        }
    }

    @get('/detail/:id')
    async getDetail(ctx, next) {
        const id = ctx.params.id
        const movie = await getMovieDetail(id)
        const relativeMovies = await getRelativeMovies(movie)

        ctx.body = {
            data: {
                movie,
                relativeMovies
            },
            success: true
        }
    }
}

module.exports = { MovieController }